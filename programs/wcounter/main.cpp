#include <cstring>
#include <errno.h>
#include <exception>
#include <fcntl.h>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <thread>

#include <uniqueword/parser.hpp>

extern int errno;

int main( int argc, char** argv )
{
  try
  {
    if( argc < 2 || argc > 3 )
      throw std::runtime_error{ "Usage: " + std::string{ argv[0] } + " <in_file> <?threads_num = 1>" };

    unsigned long threads_num = 1;
    if( argc == 3 )
      threads_num = std::stoul( argv[2] );

    std::string filename = argv[1];
    int fd               = open( filename.c_str(), O_RDONLY, 0 );
    if( fd < 0 )
      throw std::runtime_error{ "Error occured while opening \'" + filename + "\': " + std::to_string( errno ) };

    // In the task: we assume that computer memory can handle files up to 32 GiB

    struct stat stat_buf;
    if( fstat( fd, &stat_buf ) == -1 )
      throw std::runtime_error{ "Error occured while reading size of \'" + filename + "\': " + std::to_string( errno ) };
    size_t buf_size = stat_buf.st_size;

    char* buffer = static_cast< char* >( mmap( NULL, buf_size, PROT_READ, MAP_SHARED, fd, 0 ) );
    if( buffer == MAP_FAILED )
      throw std::runtime_error{ "Error occured while mapping \'" + filename + "\': " + std::to_string( errno ) };

    uniqueword::parser _parser{ threads_num };
    _parser.write( { buffer, buf_size } );

    std::thread( [&_parser] {
      while( _parser.is_runnning() )
        continue;
    } )
      .join();

    if( munmap( buffer, buf_size ) == -1 )
      throw std::runtime_error{ "Error occured while unmapping \'" + filename + "\': " + std::to_string( errno ) };

    if( close( fd ) == -1 )
      throw std::runtime_error{ "Error occured while closing \'" + filename + "\': " + std::to_string( errno ) };

#ifdef WCOUNTER_DEBUG
    for( const auto& unique: _parser.get_uniques() )
      std::cout << '\'' << unique << "\'\n";
#endif
    std::cout << _parser.get_uniques().size() << '\n';

    return 0;
  }
  catch( const std::exception& e )
  {
    std::cerr << e.what() << '\n';
  }
  catch( ... )
  {
    std::cerr << "Unexpected error occured\n";
  }

  return -1;
}
