#include <uniqueword/parser.hpp>

#include <algorithm>
#include <mutex>

namespace uniqueword {

parser::state parser::get_state() const
{
  return _state;
}

bool parser::is_runnning() const
{
  for( const auto& job: threads_jobs )
    if( job.has_value() )
      return true;
  return false;
}

const parser::underlying_t& parser::get_uniques() const
{
  return uniques;
}

parser::parser( size_t threads_num )
{
  ++threads_num;
  working_threads.reserve( threads_num );
  threads_jobs.reserve( threads_num );

  for( size_t i = 0; i < threads_num; ++i )
  {
    working_threads.emplace_back(
      [this]( size_t thread_num ) -> void {
        while( _state != state::stopped )
        {
          if( _state != state::running )
            continue;
          if( !threads_jobs.at( thread_num ).has_value() )
            continue;

          for( auto& name: parse( *threads_jobs.at( thread_num ) ) )
            add_to_uniques( std::move( name ) );

          threads_jobs.at( thread_num ).reset();
        }
      },
      i );

    threads_jobs.emplace_back();
  }

  _state = state::running;
}

parser::~parser()
{
  _state = state::stopped;

  for( auto& _thread: working_threads )
    _thread.join();
}

void parser::write( const std::span< const char >& buffer )
{
  size_t split_every = buffer.size() / ( working_threads.size() - 1 );
  size_t split_mod   = buffer.size() % ( working_threads.size() - 1 );

  size_t index = 0;
  if( split_every )
    for( size_t i = 0; i < working_threads.size() - 1; ++i )
    {
      // Starting case
      if( index != 0 )
        index += std::find( buffer.begin() + index, buffer.end(), ' ' ) - ( buffer.begin() + index );

      size_t end_index = index + split_every;
      end_index += std::find( buffer.begin() + end_index, buffer.end(), ' ' ) - ( buffer.begin() + end_index );

      if( buffer.begin() + end_index > buffer.end() )
        end_index = buffer.end() - buffer.begin();

      threads_jobs.at( i ) = buffer.subspan( index, end_index - index );

      if( buffer.begin() + end_index == buffer.end() )
      {
        split_mod = 0; // No data left to parse
        break;
      }

      index = end_index;
    }

  if( split_mod )
    threads_jobs.back() = buffer.subspan( index, buffer.end() - ( buffer.begin() + index ) );
}

std::vector< std::string > parser::parse( const std::span< const char >& span ) const
{
  std::vector< std::string > result;

  if( !span.size() )
    return result;

  auto bp         = span.begin();
  auto last_space = bp;

  do
  {
    if( *bp == ' ' )
    {
      // Handle double space case
      if( last_space != bp )
        result.emplace_back( last_space, bp );
      last_space = bp + 1;
      continue;
    }
  } while( ++bp != span.end() );

  // Handle no space at the end case
  if( *( bp - 1 ) != ' ' )
    result.emplace_back( last_space, bp );

  return result;
}

bool parser::add_to_uniques( std::string&& name )
{
  std::lock_guard< std::mutex > uniques_lock{ uniques_mutex };

  return uniques.emplace( name ).second;
}

} // namespace uniqueword
