#pragma once

#include <atomic>
#include <mutex>
#include <optional>
#include <span>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>

namespace uniqueword {

class parser final {
public:
  enum state
  {
    uninitialized, // Just for information: no particular use case (as for now)
    running,
    stopped
  };

  using underlying_t = std::unordered_set< std::string >;

  /**
   * @return current parser state
   */
  state get_state() const;

  /**
   * @return const reference to the set of parsed unique words
   */
  const underlying_t& get_uniques() const;

  /**
   * @return either true or false based on the #threads_jobs state
   */
  bool is_runnning() const;

  /**
   * @param threads_num Number of analyzer threads (defaults to 1)
   */
  parser( size_t threads_num = 1 );
  ~parser();

  /**
   * @brief Writes data to the buffer for the parser to analyze
   * @param buffer given buffer as a span
   */
  void write( const std::span< const char >& buffer );

private:
  std::atomic< state > _state = state::uninitialized;
  std::vector< std::thread > working_threads;

  // To avoid using libraries like boost for the concurrent data containers use
  // vector of optional spans where every element corresponds to the proper index of working thread
  std::vector< std::optional< std::span< const char > > > threads_jobs;

  /**
   * @brief Adds given word to the uniques set
   * @param name name of the word to insert
   * @return either true or false if the insertion occured
   */
  bool add_to_uniques( std::string&& name );

  std::vector< std::string > parse( const std::span< const char >& span ) const;

  underlying_t uniques;
  std::mutex uniques_mutex;
};

} // namespace uniqueword
