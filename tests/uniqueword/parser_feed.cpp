#include <gtest/gtest.h>

#include <thread>

#include <uniqueword/parser.hpp>

namespace {

void wait_for( const uniqueword::parser& _parser )
{
    std::thread( [&_parser] {
      while( _parser.is_runnning() )
        continue;
    } )
      .join();
}

TEST( parser_feed_tests, parse_empty )
{
  uniqueword::parser _parser;

  _parser.write( { "", 0 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 0 );

  _parser.write( { "", 0 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 0 );
}

TEST( parser_feed_tests, parse_spaces_only )
{
  uniqueword::parser _parser;

  _parser.write( { " ", 1 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 0 );

  _parser.write( { "   ", 3 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 0 );
}

TEST( parser_feed_tests, parse_one_word_one_letter )
{
  uniqueword::parser _parser;

  _parser.write( { "a", 1 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 1 );

  _parser.write( { "i", 1 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );
}

TEST( parser_feed_tests, parse_one_word_left_space )
{
  uniqueword::parser _parser;

  _parser.write( { " a", 2 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 1 );

  _parser.write( { " b", 2 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );
}

TEST( parser_feed_tests, parse_one_word_right_space )
{
  uniqueword::parser _parser;

  _parser.write( { "a ", 2 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 1 );

  _parser.write( { "b ", 2 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );
}

TEST( parser_feed_tests, parse_one_word_left_right_space )
{
  uniqueword::parser _parser;

  _parser.write( { " a ", 3 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 1 );

  _parser.write( { " b", 3 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );
}

TEST( parser_feed_tests, parse_one_word_multiple_letters )
{
  uniqueword::parser _parser;

  _parser.write( { "alice", 5 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 1 );

  _parser.write( { "bob", 3 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );
}

TEST( parser_feed_tests, parse_multiple_words_one_letter )
{
  uniqueword::parser _parser;

  _parser.write( { "b c", 3 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );

  _parser.write( { "d e", 3 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 4 );
}

TEST( parser_feed_tests, parse_multiple_word_left_right_space )
{
  uniqueword::parser _parser;

  _parser.write( { " a b ", 5 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );

  _parser.write( { " d e ", 5 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 4 );
}

TEST( parser_feed_tests, parse_multiple_words_multiple_letters )
{
  uniqueword::parser _parser;

  _parser.write( { "multiple words", 14 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 2 );

  _parser.write( { "uniqueless sentences", 20 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 4 );
}

TEST( parser_feed_tests, parse_long_text )
{
  uniqueword::parser _parser;

  _parser.write( { "this text is not so long", 24 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 6 );
}

} // namespace
