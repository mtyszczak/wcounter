#include <gtest/gtest.h>

#include <thread>

#include <uniqueword/parser.hpp>

namespace {

void wait_for( const uniqueword::parser& _parser )
{
  std::thread( [&_parser] {
    while( _parser.is_runnning() )
      continue;
  } )
    .join();
}

TEST( parser_unique_multithread_tests, parse_repeating_words )
{
  uniqueword::parser _parser{ 4 };

  _parser.write( { "this text has repeating words like alice bob and alice text bob alice", 69 } );
  wait_for( _parser );
  EXPECT_EQ( _parser.get_uniques().size(), 9 );
}

} // namespace
