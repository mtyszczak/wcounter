# wcounter
Unique word counter

## Task
Using the multithreading:
Write a program that counts the number of distinct unique words in a file whose name is passed as an argument to a program.
For example, given the file content "a horse and a dog" the program must output "4" (the word 'a' appears twice but only accounts for one distinct unique occurrence).
The input text is guaranteed to contain only 'a'..'z' and space characters in ASCII encoding.
The program should be able to handle large inputs (e.g. 32 GiB)
You can assume that all unique words fit into memory when using the data structure of your choice.
The solution must utilize all available CPU resources

## Building
### Compile-Time Options (cmake)
#### WCOUNTER_STATIC_BUILD=[ON/OFF] (OFF)
Builds wcounter as a static library instead of shared library

#### WCOUNTER_BUILD_TESTS=[ON/OFF] (OFF)
Builds wcounter tests

#### WCOUNTER_FAIL_ON_WARNINGS=[ON/OFF] (OFF)
Treat compiler warnings as errors

#### UNIQUEWORD_NO_MULTITHREAD_TESTS=[ON/OFF] (OFF)
Do not build multithread uniqueword library tests

### Building on Ubuntu 22.04

```sh
sudo apt update

# Required packages
sudo apt install -y \
    cmake \
    g++ \
    git \
    make

git clone https://gitlab.com/mtyszczak/wcounter.git
cd wcounter
git checkout main
git submodule update --init --recursive --progress

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) all

# optional
make install # defaults to /usr/local
```

## Testing
Configure the program again with the `WCOUNTER_BUILD_TESTS` CMake option enabled and then, from the `build` directory run:
```bash
tests/uniqueword/uniqueword_tests
```

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3 See [LICENSE.md](LICENSE.md)
